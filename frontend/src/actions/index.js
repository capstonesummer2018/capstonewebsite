import apiCall from "./../utils/apiCaller";
import * as Types from "./../constants/ActionTypes";

export const getCodeRequest = () => {
  return dispatch => {
    return apiCall("code", "GET", null, null).then(res => {
      dispatch(getCode(res.data));
    });
  };
};

export const getCode = codelist => {
  return {
    type: Types.GET_CODE,
    codelist
  };
};

export const addCodeRequest = code => {
  return dispatch => {
    return apiCall("code", "POST", code, null).then(res => {
      dispatch(addCode(res.data));
    });
  };
};

export const addCode = code => {
  return {
    type: Types.ADD_CODE,
    code
  };
};
export const deleteCodeRequest = id => {
  return dispatch => {
    return apiCall(`code/${id}`, "DELETE", null, null).then(res => {
      dispatch(deleteCode(id));
    });
  };
};

export const deleteCode = id => {
  return {
    type: Types.DELETE_CODE,
    id: id
  };
};

export const searchCode = filter => {
  return {
    type: Types.SEARCH_CODE,
    filter: filter
  };
};

export const getCodeIdRequest = codeId => {
  return dispatch => {
    return apiCall(`code/${codeId}`, "GET", null, null).then(res => {
      dispatch(getCodeId(res.data));
    });
  };
};

export const getCodeId = codeIdItem => {
  return {
    type: Types.GET_CODE_ID,
    codeIdItem
  };
};

export const getSampleByCodeIdRequest = codeId => {
  return dispatch => {
    return apiCall(`code/${codeId}/sample`, "GET", null, null).then(res => {
      dispatch(getSampleByCodeId(res.data));
    });
  };
};

export const getSampleByCodeId = samplelistbycodeid => {
  return {
    type: Types.GET_SAMPLE_BY_CODE_ID,
    samplelistbycodeid
  };
};

export const addSampleRequest = (sampleItem, codeId) => {
  return dispatch => {
    return apiCall(`code/${codeId}/sample`, "POST", sampleItem, null).then(
      res => {
        dispatch(addSample(res.data));
      }
    );
  };
};

export const addSample = sampleItem => {
  return {
    type: Types.ADD_SAMPLE,
    sampleItem
  };
};

export const getSampleIdRequest = (codeId, sampleId) => {
  return dispatch => {
    return apiCall(`code/${codeId}/sample/${sampleId}`, "GET", null, null).then(
      res => {
        dispatch(getSampleId(res.data));
      }
    );
  };
};

export const getSampleId = sampleIdItem => {
  return {
    type: Types.VIEW_SAMPLE,
    sampleIdItem
  };
};

// export const getSampleStatus = sampleStatus => {
//   return {
//     type: Types.GET_SAMPLE_STATUS,
//     sampleStatus
//   };
// };

export const updateStatusSampleIdRequest = (status, codeid, sampleid) => {
  return dispatch => {
    return apiCall(`code/${codeid}/sample/${sampleid}`, "PUT", status, null);
    // .then(res => {
    //   dispatch(updateStatusSampleId(status));
    // });
  };
};

// export const updateStatusSampleId = sampleStatusId => {
//   return {
//     type: Types.UPDATE_SAMPLE_STATUS,
//     sampleStatusId
//   };
// };

export const searchSample = keyword => {
  return {
    type: Types.SEARCH_SAMPLE,
    keyword
  };
};

export const updateStatusCodeIdRequest = (status, codeId) => {
  return dispatch => {
    return apiCall(`code/${codeId}`, "PUT", status, null);
  };
};

// export const updateStatusCodeId = statusCode => {
//   return {
//     type: Types.UPDATE_CODE_STATUS,
//     statusCode
//   };
// };
