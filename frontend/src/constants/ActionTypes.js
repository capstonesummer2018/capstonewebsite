export const GET_CODE = "GET_CODE";
export const GET_CODE_ID = "GET_CODE_ID";
export const ADD_CODE = "ADD_CODE";
export const DELETE_CODE = "DELETE_CODE";
export const UPDATE_CODE = "UPDATE_CODE";
export const EDIT_CODE = "EDIT_CODE";
export const SEARCH_CODE = "SEARCH_CODE";
// export const UPDATE_CODE_STATUS = "UPDATE_CODE_STATUS";
export const GET_SAMPLE_BY_CODE_ID = "GET_SAMPLE_BY_CODE_ID";
export const ADD_SAMPLE = "ADD_SAMPLE";
export const DELETE_SAMPLE = "DELETE_SAMPLE";
export const UPDATE_SAMPLE = "UPDATE_SAMPLE";
export const EDIT_SAMPLE = "EDIT_SAMPLE";
export const SEARCH_SAMPLE = "SEARCH_SAMPLE";
export const VIEW_SAMPLE = "VIEW_SAMPLE";
// export const GET_SAMPLE_STATUS = "GET_SAMPLE_STATUS";
// export const UPDATE_SAMPLE_STATUS = "UPDATE_SAMPLE_STATUS";

