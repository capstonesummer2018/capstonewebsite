import React, { Component, Fragment } from "react";
import "./StepElectro.css";
class StepElectro extends Component {
  render() {
    if (this.props.currentStep !== 3) {
      return null;
    }
    return (
      <Fragment>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            className="form-control"
            id="password"
            name="password"
            type="text"
            placeholder="Enter password"
            value={this.props.password} // Prop: The email input data
            onChange={this.props.handleChange} // Prop: Puts data into state
          />
        </div>
       
      </Fragment>
    );
  }
}

export default StepElectro;
