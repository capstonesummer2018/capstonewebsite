import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import moment from "moment";

import { connect } from "react-redux";
import "./DashboardListCodeItem.css";
import { deleteCodeRequest } from "../../actions";
class DashboardListCodeItem extends Component {
  onDelete = id => {
    this.props.onDeleteCode(id);
  };

  render() {
    var { code } = this.props;

    return (
      <tr>
        <td style={{ fontWeight: "500" }}>{code.codeCA}</td>
        <td style={{ fontWeight: "500" }}>{code.organization}</td>
        <td>{code.qdtcNumber}</td>
        <td>{moment(code.receiveDate).format("DD/MM/YYYY, hh:mm a")}</td>

        <td>{moment(code.signDate).format("DD/MM/YYYY, hh:mm a")}</td>
        <td>
          <div
            className={`status-wrapper ${
              code.status === true
                ? "done"
                : code.status === false
                ? "notyet"
                : "doing"
            }`}
          >
            {code.status === true
              ? "Done"
              : code.status === false
              ? "Not yet"
              : "Doing"}
          </div>
        </td>
        <td>
          <div className="action-wrapper d-flex justify-content-center align-items-center">
            <div className="edit-btn mr-2">
              <NavLink to={`/dashboard/code/${code.id}`}>View Sample</NavLink>
            </div>
            <div className="delete-btn">
              <button
                type="button"
                className="btn custom-btn-delete"
                onClick={() => this.onDelete(code.id)}
              >
                Delete
              </button>
            </div>
          </div>
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onDeleteCode: id => {
      dispatch(deleteCodeRequest(id));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(DashboardListCodeItem);
