import React, { Component } from "react";
import "./StepExtract.css";
class StepExtract extends Component {
  render() {
    if (this.props.currentStep !== 1) {
      // Prop: The current step
      return null;
    }
    return (
      <div className="row">
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="gdcSymbol" className="input-label">
              GDC Symbol
            </label>
            <input
              className="form-control"
              id="gdcSymbol"
              name="gdcSymbol"
              type="text"
              value={this.props.gdcSymbol} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="theExecutor" className="input-label">
              The Executor
            </label>
            <input
              className="form-control"
              id="theExecutor"
              name="theExecutor"
              type="text"
              value={this.props.theExecutor} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="extrationKit" className="input-label">
              Extration Kit
            </label>
            <input
              className="form-control"
              id="extrationKit"
              name="extrationKit"
              type="text"
              value={this.props.extrationKit} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="extrationSystem" className="input-label">
              Extraction System
            </label>
            <input
              className="form-control"
              id="extrationSystem"
              name="extrationSystem"
              type="text"
              value={this.props.extrationSystem} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="adnSystem" className="input-label">
              ADN System
            </label>
            <input
              className="form-control"
              id="adnSystem"
              name="adnSystem"
              type="text"
              value={this.props.adnSystem} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="contentNGML" className="input-label">
              Content ( ng/ml )
            </label>
            <input
              className="form-control"
              id="contentNGML"
              name="contentNGML"
              type="text"
              value={this.props.contentNGML} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="start" className="input-label">
              Start
            </label>
            <input
              className="form-control"
              id="start"
              name="start"
              type="text"
              value={this.props.start} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label htmlFor="end" className="input-label">
              End
            </label>
            <input
              className="form-control"
              id="end"
              name="end"
              type="text"
              value={this.props.end} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        </div>
        <div className="col-lg-12">
          <div className="form-group">
            <label htmlFor="exampleFormControlTextarea1" className="input-label">Description</label>
            <textarea
              className="form-control"
              name="description"
              id="exampleFormControlTextarea1"
              rows="3"
              value={this.props.description}
              onChange={this.props.handleChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default StepExtract;
