import React, { Component, Fragment } from "react";
import { Route, Switch } from "react-router-dom";
import "./DashboardContent.css";
import "./../../../node_modules/perfect-scrollbar/css/perfect-scrollbar.css";
// import PerfectScrollbar from "perfect-scrollbar";
import DashboardHomePage from "./../DashboardHomePage/DashboardHomePage";
import DashboardTopMenu from "./../DashboardTopMenu/DashboardTopMenu";
import DashboardShowListCode from "./../DashboardShowListCode/DashboardShowListCode";
import DashboardListCodeAdd from "./../DashboardListCodeAdd/DashboardListCodeAdd";
import DashboardListSampleAdd from "./../DashboardListSampleAdd/DashboardListSampleAdd";
import DashboardListSampleAddStep from "./../DashboardListSampleAddStep/DashboardListSampleAddStep";
import DashboardViewSample from "./../DashboardViewSample/DashboardViewSample";
import DashboardShowListSample from "./../DashboardShowListSample/DashboardShowListSample";
import DashboardListConclusion from "./../DashboardListConclusion/DashboardListConclusion";
import DashboardListLitigant from "./../DashboardListLitigant/DashboardListLitigant";
import DashboardListLitigantAdd from "./../DashboardListLitigantAdd/DashboardListLitigantAdd";

import { CSSTransition, TransitionGroup } from "react-transition-group";
class DashboardContent extends Component {
  // componentDidMount() {
  //   new PerfectScrollbar(".dashboard-content", {
  //     // wheelPropagation: true,
  //     maxScrollbarLength: 200
  //     // swipeEasing :true
  //   });
  // }
  render() {
    return (
      <Fragment>
        <div className="dashboard-content">
          <DashboardTopMenu />
          {/* <div className="dashboard-content-wrapper"> */}
          <Route
            render={({ location, match }) => (
              <TransitionGroup className="dashboard-content-wrapper">
                <CSSTransition
                  key={location.key}
                  timeout={{ enter: 300, exit: 300 }}
                  classNames="fade"
                >
                  <Switch location={location} match={match}>
                    <Route
                      path="/dashboard"
                      exact
                      component={DashboardHomePage}
                    />
                    <Route
                      path="/dashboard/code"
                      exact
                      component={DashboardShowListCode}
                    />
                    <Route
                      path="/dashboard/code/add"
                      component={DashboardListCodeAdd}
                    />
                    <Route
                      path="/dashboard/code/:codeid/sample/:sampleid"
                      exact
                      component={DashboardListSampleAddStep}
                    />
                    <Route
                      path="/dashboard/code/:codeid/add"
                      component={DashboardListSampleAdd}
                    />
                    <Route
                      path="/dashboard/code/:codeid"
                      exact
                      component={DashboardShowListSample}
                    />
                    <Route
                      path="/dashboard/code/:codeid/sample/:sampleid/view"
                      component={DashboardViewSample}
                    />
                    <Route
                      path="/dashboard/conclusion"
                      component={DashboardListConclusion}
                    />
                    <Route
                      path="/dashboard/litigant"
                      exact
                      component={DashboardListLitigant}
                    />
                    <Route
                      path="/dashboard/litigant/add"
                      component={DashboardListLitigantAdd}
                    />
                  </Switch>
                </CSSTransition>
              </TransitionGroup>
            )}
          />
        </div>
        {/* </div> */}
      </Fragment>
    );
  }
}

export default DashboardContent;
