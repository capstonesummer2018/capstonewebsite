import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Pagination from "react-js-pagination";
import DashboardListSample from "./../DashboardListSample/DashboardListSample";
import DashboardListSampleItem from "./../DashboardListSampleItem/DashboardListSampleItem";
import {
  updateStatusCodeIdRequest,
  getSampleByCodeIdRequest
} from "../../actions";
class DashboardShowListSample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      itemPerPage: 5,
      status: null,
      countStatus: null,
      filter: []
    };
  }

  componentDidMount() {
    var { match } = this.props;
    this.props.onGetSampleByCodeId(match.params.codeid);
  }

  onShowCodeList = samplelistbycodeid => {
    var { match } = this.props;
    var result = null;
    result = samplelistbycodeid.map((sample, index) => {
      return (
        <DashboardListSampleItem key={index} sample={sample} match={match} />
      );
    });
    return result;
  };

  handlePageChange = pageNumber => {
    this.setState({ activePage: pageNumber });
  };

  render() {
    var { samplelistbycodeid, match, filtersample } = this.props;
    if (filtersample) {
      if (filtersample.keyword) {
        samplelistbycodeid = samplelistbycodeid.filter(sample => {
          return sample.code.toString().indexOf(filtersample.keyword) !== -1;
        });
      }
    }

    var trueCount = 0;
    var falseCount = 0;
    var nullCount = 0;
    samplelistbycodeid.filter(a => {
      return a.status === true
        ? trueCount++
        : a.status === false
        ? falseCount++
        : nullCount++;
    });

    if (trueCount === samplelistbycodeid.length) {
      this.props.onChangeStatusCodeId({ status: true }, match.params.codeid);
    } else if (falseCount === samplelistbycodeid.length) {
      this.props.onChangeStatusCodeId({ status: false }, match.params.codeid);
    } else {
      this.props.onChangeStatusCodeId({ status: null }, match.params.codeid);
    }
    return (
      <Fragment>
        <DashboardListSample match={match}>
          {this.onShowCodeList(samplelistbycodeid)}
        </DashboardListSample>
        <div className="d-flex justify-content-center">
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={5}
            totalItemsCount={samplelistbycodeid.length}
            pageRangeDisplayed={5}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    codeiditem: state.codeiditem,
    samplelistbycodeid: state.samplelistbycodeid,
    filtersample: state.filtersample
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetSampleByCodeId: codeId => {
      dispatch(getSampleByCodeIdRequest(codeId));
    },
    onChangeStatusCodeId: (status, codeId) => {
      dispatch(updateStatusCodeIdRequest(status, codeId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardShowListSample);
