import React, { Component } from "react";
import "./DashboardTopMenu.css";
import { NavLink } from "react-router-dom";
class DashboardTopMenu extends Component {
  componentDidMount() {
    const $ = window.$;
    $("#toggle-menu").on("click", () => {
      $(".dashboard-menu-left").toggleClass("show");
      $(".overlay").toggleClass("show");
    });
    $(".overlay").on("click", () => {
      $(".dashboard-menu-left").removeClass("show");
      $(".overlay").removeClass("show");
    });
  }
  render() {
    return (
      <div className="dashboard-top-menu">
        <nav className="navbar navbar-expand-lg custom-navtop">
          <button id="toggle-menu">
            <i className="fas fa-bars" />
          </button>
          <div className="left-btn d-flex pl-3 mr-auto">
            {/* <div className="btn-add-code">
              <NavLink to="/dashboard/code/add">
                <i className="fas fa-plus mr-2" />Add New Code
              </NavLink>
            </div> */}
          </div>
          <ul className="nav d-flex align-items-center ml-auto">
            <li className="nav-item">
              <div className="dropdown">
                <button
                  className="notice"
                  type="button"
                  id="dropdownMenuButton2"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <span className="mr-4">
                    <span>
                      <i className="far fa-bell" />
                    </span>
                    <span className="badge badge-custom">5</span>
                  </span>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton2"
                  >
                    <NavLink className="dropdown-item" exact to="/dashboard">
                      <span>
                        <i className="far fa-user" />
                      </span>
                      <span className="ml-2">Profile</span>
                    </NavLink>

                    <div className="dropdown-divider" />
                    <NavLink className="dropdown-item" to="/dashboard/add">
                      <span>
                        <i className="fas fa-sign-out-alt" />
                      </span>
                      <span className="ml-2">Logout</span>
                    </NavLink>
                  </div>
                </button>
              </div>
            </li>
            <li className="nav-item">
              <div className="profile">
                <div className="dropdown">
                  <button
                    className="button-profile dropdown-toggle d-flex align-items-center"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <div className="image-profile">
                      <img src="https://picsum.photos/id/220/45/45" alt="aa" />
                    </div>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="dropdownMenuButton"
                    >
                      <NavLink className="dropdown-item" exact to="/dashboard">
                        <span>
                          <i className="far fa-user" />
                        </span>
                        <span className="ml-2">Profile</span>
                      </NavLink>

                      <div className="dropdown-divider" />
                      <NavLink className="dropdown-item" to="/dashboard/add">
                        <span>
                          <i className="fas fa-sign-out-alt" />
                        </span>
                        <span className="ml-2">Logout</span>
                      </NavLink>
                    </div>
                    <div className="text-profile">
                      <p className="font-weight-bold">Nathalie Roberts</p>
                    </div>
                  </button>
                </div>
              </div>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default DashboardTopMenu;
