import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import Steps, { Step } from "rc-steps";
import "rc-steps/assets/index.css";
import "rc-steps/assets/iconfont.css";
import StepExtract from "./../StepExtract/StepExtract";
import StepElectro from "./../StepElectro/StepElectro";
import StepPRC from "./../StepPRC/StepPRC";
import StepPurify from "./../StepPurify/StepPurify";
import StepResult from "./../StepResult/StepResult";
import "./DashboardListSampleAddStep.css";
import { getSampleIdRequest, updateStatusSampleIdRequest } from "../../actions";
class DashboardListSampleAddStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      steps: [
        {
          title: "Extract Step"
        },
        {
          title: "PRC Step"
        },
        {
          title: "Electrophoresis Step"
        },
        {
          title: "Purify Step"
        },
        {
          title: "Result Step"
        }
      ],
      currentStep: 1, // Default is Step 1
      id: "",
      gdcSymbol: "",
      theExecutor: "",
      extrationKit: "",
      extrationSystem: "",
      adnSystem: "",
      contentNGML: "",
      start: "",
      end: "",
      description: "",
      done: null
    };
  }

  componentDidMount() {
    var { match } = this.props;
    this.props.onGetSampleId(match.params.codeid, match.params.sampleid);
  }
  next = () => {
    var { match } = this.props;

    let currentStep = this.state.currentStep;
    currentStep = currentStep >= 4 ? 5 : currentStep + 1;
    this.setState({
      currentStep: currentStep
    });
    var status = {
      status: null
    };
    this.props.onUpdateStatusSampleId(
      status,
      match.params.codeid,
      match.params.sampleid
    );
  };

  prev = () => {
    let { currentStep } = this.state;
    currentStep = currentStep <= 1 ? 1 : currentStep - 1;
    this.setState({
      currentStep: currentStep
    });
  };
  prevBtn = () => {
    let { currentStep } = this.state;
    if (currentStep !== 1) {
      return (
        <button type="button" className="btn btn-primary" onClick={this.prev}>
          Back
        </button>
      );
    } else {
      return null;
    }
  };

  nextBtn = () => {
    let { currentStep } = this.state;
    if (currentStep < 5) {
      return (
        <button
          type="button"
          className="btn btn-success ml-auto"
          onClick={this.next}
        >
          Next
        </button>
      );
    } else {
      return null;
    }
  };
  submitBtn = () => {
    let { currentStep } = this.state;
    if (currentStep === 5) {
      return (
        <button
          type="button"
          className="btn btn-success ml-auto"
          onClick={this.handleSubmit}
        >
          Submit
        </button>
      );
    } else {
      return null;
    }
  };
  handleChange = e => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({
      [name]: value
    });
    console.log(this.state.done);
  };

  handleSubmit = e => {
    var { match } = this.props;

    e.preventDefault();
    const {
      gdcSymbol,
      theExecutor,
      extrationKit,
      extrationSystem,
      adnSystem,
      contentNGML,
      start,
      end,
      description
    } = this.state;
    alert(`Your submit: \n
      gdcSymbol: ${gdcSymbol}
      theExecutor: ${theExecutor}
      extrationKit: ${extrationKit}
      extrationSystem: ${extrationSystem}
      adnSystem: ${adnSystem}
      contentNGML: ${contentNGML}
      start: ${start}
      end: ${end}
      description: ${description}`);

    var status = {
      status: this.state.done === "1" ? true : null
    };
    this.props.onUpdateStatusSampleId(
      status,
      match.params.codeid,
      match.params.sampleid
    );
  };

  setCurrent = index => {
    this.setState({
      currentStep: index
    });
  };
  render() {
    var { match } = this.props;

    this.stepsRefs = [];
    var { currentStep } = this.state;
    var { steps } = this.state;
    return (
      <div className="card card-custom2">
        <div className="card-title mt-5">
          <div className="exit-btn">
            <NavLink to={`/dashboard/code/${match.params.codeid}`}>
              <i className="fas fa-times" />
            </NavLink>
          </div>
          <Steps labelPlacement="vertical" current={currentStep - 1}>
            {steps.map((step, index) => {
              return (
                <Step
                  ref={c => (this.stepsRefs[index] = c)}
                  key={index}
                  title={step.title}
                  onClick={() => this.setCurrent(index + 1)}
                />
              );
            })}
          </Steps>
        </div>
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <StepExtract
              currentStep={currentStep}
              handleChange={this.handleChange}
              gdcSymbol={this.state.gdcSymbol}
              theExecutor={this.state.theExecutor}
              extrationKit={this.state.extrationKit}
              extrationSystem={this.state.extrationSystem}
              adnSystem={this.state.adnSystem}
              contentNGML={this.state.contentNGML}
              start={this.state.start}
              end={this.state.end}
              description={this.state.description}
            />
            <StepPRC
              currentStep={currentStep}
              handleChange={this.handleChange}
              username={this.state.username}
            />
            <StepElectro
              currentStep={currentStep}
              handleChange={this.handleChange}
              password={this.state.password}
            />
            <StepPurify
              currentStep={currentStep}
              handleChange={this.handleChange}
              phone={this.state.phone}
            />
            <StepResult
              currentStep={currentStep}
              handleChange={this.handleChange}
              description={this.state.description}
            />
            <div className="navigate-btn d-flex justify-content-between ">
              {this.prevBtn()}
              {this.nextBtn()}
              {this.submitBtn()}
            </div>
          </form>

          {/* <StepPurify
              currentStep={currentStep}
              handleChange={this.handleChange}
              email={this.state.email}
            /> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    sampleiditem: state.sampleiditem
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetSampleId: (codeId, sampleId) => {
      dispatch(getSampleIdRequest(codeId, sampleId));
    },
    onUpdateStatusSampleId: (status, codeId, sampleId) => {
      dispatch(updateStatusSampleIdRequest(status, codeId, sampleId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardListSampleAddStep);
