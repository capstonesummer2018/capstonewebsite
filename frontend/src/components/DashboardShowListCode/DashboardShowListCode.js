import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Pagination from "react-js-pagination";
import "./DashboardShowListCode.css";
import DashboardListCode from "./../DashboardListCode/DashboardListCode";
import DashboardListCodeItem from "./../DashboardListCodeItem/DashboardListCodeItem";
import { getCodeRequest } from "../../actions";
class DashboardShowListCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      itemPerPage: 5
    };
  }

  
  componentDidMount() {
    this.props.onGetCodeList();
   
  }
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  };
  onShowCodeList = codelist => {
    var result = null;
    if (codelist.length > 0) {
      result = codelist.map((code, index) => {
        return <DashboardListCodeItem key={index} code={code} />;
      });
      return result;
    }
  };

  render() {
    var { codelist, filter } = this.props;

    if (filter) {
      if (filter.name) {
        codelist = codelist.filter((code, index) => {
          return code.code.toString().indexOf(filter.name) !== -1;
        });
      }
    }
    var indexOfLastTodo = this.state.activePage * this.state.itemPerPage;
    var indexOfFirstTodo = indexOfLastTodo - this.state.itemPerPage;
    var renderedProjects = codelist.slice(indexOfFirstTodo, indexOfLastTodo);

    return (
      <Fragment>
        <DashboardListCode>
          {this.onShowCodeList(renderedProjects)}
        </DashboardListCode>
        <div className="d-flex justify-content-center">
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={5}
            totalItemsCount={codelist.length}
            pageRangeDisplayed={5}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange.bind(this)}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    codelist: state.codelist,
    filter: state.filter
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetCodeList: () => {
      dispatch(getCodeRequest());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardShowListCode);
