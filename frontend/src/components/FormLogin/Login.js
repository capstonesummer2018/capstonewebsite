import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./../../resources/css/main.css";
class Login extends Component {
  render() {
    return (
      
      <div className="limiter">
        <div className="container-login100">
          <div className="wrap-login100">
            <form className="login100-form validate-form">
              <span className="login100-form-title mb-5">DNA Testing Process Management</span>
              <span className="login100-form-title custom-icon-dna mb-5">
              <i className="fas fa-dna"></i>
              </span>

              <div
                className="wrap-input100 validate-input"
                data-validate="Valid email is: a@b.c"
              >
                <input className="input100" type="text" name="email" />
                <span className="focus-input100" data-placeholder="Email" />
              </div>

              <div
                className="wrap-input100 validate-input"
                data-validate="Enter password"
              >
                <span className="btn-show-pass">
                  <i className="zmdi zmdi-eye" />
                </span>
                <input className="input100" type="password" name="pass" />
                <span className="focus-input100" data-placeholder="Password" />
              </div>

              <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                  <div className="login100-form-bgbtn" />
                  <NavLink className="login100-form-btn" to="/dashboard">
                    Login
                  </NavLink>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
