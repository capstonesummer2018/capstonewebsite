import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import "./DashboardListSample.css";
import { getCodeIdRequest, searchSample } from "../../actions";
class DashboardListSample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: ""
    };
  }
  
  onChange = e => {
    const { value, name } = e.target;
    this.props.onFilter({
      keyword: name === "keyword" ? value : this.state.keyword
    });
    this.setState({
      [name]: value
    });
  };
  render() {
    var { match } = this.props;

    return (
      <Fragment>
        <div className="list-code-wrapper">
          <div className="breadcrumb-wrapper d-flex justify-content-start align-items-center mb-5">
            <div className="breadcrumb-header">
              <h2> View Samples</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <NavLink to="/dashboard">
                    <i className="fas fa-home" />
                  </NavLink>
                </li>
                <li className="breadcrumb-item">
                  <NavLink to="/dashboard">Manage Case</NavLink>
                </li>
                <li className="breadcrumb-item">
                  <NavLink to="/dashboard/code">View List Case</NavLink>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  View Samples
                </li>
              </ol>
            </nav>
          </div>
          <div className="list-code-option-bar-wrapper d-flex justify-content-between align-items-center">
            <div className="add-option">
              <div className="btn-add-code">
                <NavLink to={`/dashboard/code/${match.params.codeid}/add`}>
                  <i className="fas fa-plus mr-2" />
                  Add New Sample
                </NavLink>
              </div>
            </div>

            <div className="search-option">
              <input
                type="text"
                name="keyword"
                className="input-search"
                onChange={this.onChange}
                placeholder="Search sample code"
              />
              <button type="button">
                <i className="fas fa-search" onClick={this.onSearch} />
              </button>
            </div>
          </div>

          <div className="list-code-content">
            <div className="table-responsive">
              <table className="table table-borderless table-custom">
                <thead>
                  <tr className="text-center">
                    <th scope="col">Code</th>
                    <th scope="col">Collector</th>

                    <th scope="col">Deliverer</th>

                    <th scope="col">Receiver</th>

                    <th scope="col">Start Date</th>
                    <th scope="col">End Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>{this.props.children}</tbody>
              </table>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    codeiditem: state.codeiditem
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetIdItem: codeId => {
      dispatch(getCodeIdRequest(codeId));
    },
    onFilter: keyword => {
      dispatch(searchSample(keyword));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardListSample);
