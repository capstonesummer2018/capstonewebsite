import React, { Component, Fragment } from "react";
import "./StepResult.css";
class StepResult extends Component {
  render() {
    if (this.props.currentStep !== 5) {
      return null;
    }
    return (
      <Fragment>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            className="form-control"
            id="description"
            name="description"
            type="text"
            placeholder="Enter Description"
            value={this.props.description} // Prop: The email input data
            onChange={this.props.handleChange} // Prop: Puts data into state
          />
        </div>
        <div className="form-group">
          <div className="custom-control custom-checkbox ">
            <input
              className="custom-control-input"
              type="checkbox"
              value="1"
              name="done"
              onChange={this.props.handleChange}
              id="customCheck1"
            />
            <label className="custom-control-label d-flex align-items-center" htmlFor="customCheck1">
              Đã hoàn thành
            </label>
          </div>
        </div>
        {/* <button className="btn btn-success btn-block">Sign up</button> */}
      </Fragment>
    );
  }
}

export default StepResult;
