import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import DatePicker from "react-datepicker";
import { connect } from "react-redux";
import Dropzone from "react-dropzone-uploader";
// import Select from "react-select";
import axios from "axios";
import "react-dropzone-uploader/dist/styles.css";
import "react-datepicker/dist/react-datepicker.css";
import "./DashboardListSampleAdd.css";
import { getCodeIdRequest, addSampleRequest } from "../../actions";
const $ = window.$;
class DashboardListSampleAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      note: "",
      status: false,
      sampleImage: "",
      collectDate: "",
      collector: "",
      receiveDate: "",
      receiver: "",
      deliverDate: "",
      deliverer: "",
      userId: "",
      caseId: "",
      litigantId: "",
      typeId: "",
      gdgSignal: "",
      amount: "",
      startDate: "",
      endDate: "",
      descriptionMan: "",
      photoTaker: "",
      description: "",

      files: [],

      selectedOption: null,
      isClearable: true
    };
  }
  
  componentDidMount() {
    var { match } = this.props;
    
    this.props.onGetCodeId(match.params.codeid);
    $(".form-control").each(function() {
      $(this).on("keyup", function() {
        if (
          $(this)
            .val()
            .trim() !== ""
        ) {
          $(this).addClass("has-val");
          $(this)
            .next("span")
            .css({ visibility: "hidden", opacity: "0" });
        } else {
          $(this).removeClass("has-val");
          $(this)
            .next("span")
            .css({ visibility: "visible", opacity: "1" });
        }
      });
    });
  }
  onChange = e => {
    var target = e.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  };
  handleChangeStartDate = date => {
    this.setState({
      startDate: date
    });
  };
  handleChangeReceiveDate = date => {
    this.setState({
      receiveDate: date
    });
  };
  handleChangeEndDate = date => {
    this.setState({
      endDate: date
    });
  };
  handleChangeCollectDate = date => {
    this.setState({
      collectDate: date
    });
  };
  handleChangeDeliverDate = date => {
    this.setState({
      deliverDate: date
    });
  };

  onSubmit = e => {
    var {match,history} = this.props;
    const { codeiditem } = this.props;
    e.preventDefault();
    const {
      code,
      note,
      collector,
      receiver,
      deliverer,
      photoTaker,
      description,
      startDate,
      endDate,
      // files,
      collectDate,
      receiveDate,
      deliverDate,
      status
    } = this.state;
 
    var sampleItem = {
      code: code,
      note: note,
      status:status,
      sampleImage: "",
      collectDate: collectDate,
      collector: collector,
      receiveDate: receiveDate,
      receiver: receiver,
      deliverDate: deliverDate,
      deliverer: deliverer,
      userId: "",
      caseId: codeiditem.id,
      litigantId: "",
      typeId: "",
      gdgSignal: "",
      amount: "",
      startDate: startDate,
      endDate: endDate,
      descriptionMan: "",
      photoTaker: photoTaker,
      description: description,
      codeCA: codeiditem.codeCA
    };
    this.props.onAddSample(sampleItem,match.params.codeid);
    history.push(`/dashboard/code/${codeiditem.id}`);
  };
  render() {
    var { codeiditem } = this.props;
    const getUploadParams = ({ file, meta }) => {
      return axios({
        method: "POST",
        url: "https://5d009f68d021760014b75103.mockapi.io/image",
        data: meta
      });
    };

    const handleChangeStatus = ({ meta, file }, status) => {
      // console.log(status, meta, file);
    };

    const handleSubmit = (files, allFiles) => {
      allFiles.forEach(f => f.remove());
    };
    // const { selectedOption, isClearable } = this.state;
    return (
      <Fragment>
        <div className="card card-custom" style={{ marginTop: "100px" }}>
          <div className="card-body">
            <div className="card-title">Add Sample</div>
            <form onSubmit={this.onSubmit}>
              <div className="row mb-3">
              <div className="col-lg-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="code"
                      //   pattern="[0-9][0-9]{3}"
                      required
                      onChange={this.onChange}
                    />
                    <span className="input-placeholder">Code</span>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="note"
                      //   pattern="[0-9][0-9]{3}"
                      required
                      onChange={this.onChange}
                    />
                    <span className="input-placeholder">Note</span>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="collector"
                      required
                      //   pattern="[1-9][0-9]{8}"
                      onChange={this.onChange}
                    />

                    <span className="input-placeholder">Collector</span>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="receiver"
                      required
                      //   pattern="[1-9][0-9]{8}"
                      onChange={this.onChange}
                    />

                    <span className="input-placeholder">Receiver</span>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="deliverer"
                      required
                      //   pattern="[1-9][0-9]{8}"
                      onChange={this.onChange}
                    />

                    <span className="input-placeholder">Deliverer</span>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="photoTaker"
                      required
                      //   pattern="[1-9][0-9]{8}"
                      onChange={this.onChange}
                    />

                    <span className="input-placeholder">Photo Taker</span>
                  </div>
                </div>
                
                {/* <div className="col-lg-12">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="txtOrganization"
                      onChange={this.onChange}
                      required
                    />
                    <Select
                      value={selectedOption}
                      onChange={this.handleChangeSelect}
                      name="concac"
                      isClearable={isClearable}
                      options={options}
                      placeholder="Organization"
                    />
                    <span className="input-placeholder">Organization</span>
                  </div>
                </div> */}
                <div className="col-lg-6">
                  <div className="form-group">
                    <DatePicker
                      selected={this.state.startDate}
                      onChange={this.handleChangeStartDate}
                      showTimeSelect
                      showTimeInput
                      timeInputLabel="Time:"
                      timeFormat="HH:mm"
                      timeIntervals={15}
                      dateFormat="dd/MM/yyyy h:mm aa"
                      timeCaption="time"
                      placeholderText="Start Date"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      className="form-control"
                      required
                    />
                  </div>
                </div>
                
                <div className="col-lg-6">
                  <div className="form-group">
                    <DatePicker
                      selected={this.state.endDate}
                      onChange={this.handleChangeEndDate}
                      showTimeSelect
                      showTimeInput
                      timeInputLabel="Time:"
                      timeFormat="HH:mm"
                      timeIntervals={15}
                      dateFormat="dd/MM/yyyy h:mm aa"
                      timeCaption="time"
                      placeholderText="End Date"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      className="form-control"
                      required
                    />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <DatePicker
                      selected={this.state.collectDate}
                      onChange={this.handleChangeCollectDate}
                      showTimeSelect
                      showTimeInput
                      timeInputLabel="Time:"
                      timeFormat="HH:mm"
                      timeIntervals={15}
                      dateFormat="dd/MM/yyyy h:mm aa"
                      timeCaption="time"
                      placeholderText="Collect Date"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      className="form-control"
                      required
                    />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <DatePicker
                      selected={this.state.deliverDate}
                      onChange={this.handleChangeDeliverDate}
                      showTimeSelect
                      showTimeInput
                      timeInputLabel="Time:"
                      timeFormat="HH:mm"
                      timeIntervals={15}
                      dateFormat="dd/MM/yyyy h:mm aa"
                      timeCaption="time"
                      placeholderText="Deliver Date"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      className="form-control"
                      required
                    />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <DatePicker
                      selected={this.state.receiveDate}
                      onChange={this.handleChangeReceiveDate}
                      showTimeSelect
                      showTimeInput
                      timeInputLabel="Time:"
                      timeFormat="HH:mm"
                      timeIntervals={15}
                      dateFormat="dd/MM/yyyy h:mm aa"
                      timeCaption="time"
                      placeholderText="Receive Date"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      className="form-control"
                      required
                    />
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="form-group">
                    <textarea
                      type="text"
                      className="form-control"
                      name="description"
                      required
                      //   pattern="[1-9][0-9]{8}"
                      onChange={this.onChange}
                    />

                    <span className="input-placeholder">Description</span>
                  </div>
                </div>
                
                <div className="col-lg-12">
                  <Dropzone
                    getUploadParams={getUploadParams}
                    onChangeStatus={handleChangeStatus}
                    onSubmit={handleSubmit}
                    accept="image/*,audio/*,video/*"
                  />
                </div>
                
              </div>

              <button type="submit" className="btn custom-button-1 mr-3">
                Submit
              </button>
              <NavLink
                to={`/dashboard/code/${codeiditem.id}`}
                className="btn custom-button-2"
              >
                Back
              </NavLink>
              <div />
              {/* <div className="form-group">
                <input
                  type="file"
                  className="form-control"
                  accept='image/*'
                  onChange={this.handleImageChange}
                  multiple
                />
              
              </div> */}
            </form>
            {/* {imagesPreviewUrls.map(function(imagePreviewUrl, i){
                    return <img key={i} src={imagePreviewUrl} alt="" />
                })} */}
          </div>
        </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    codeiditem: state.codeiditem
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetCodeId: codeId => {
      dispatch(getCodeIdRequest(codeId));
    },
    onAddSample: (sampleItem,codeId) => {
      dispatch(addSampleRequest(sampleItem,codeId));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardListSampleAdd);
