import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./DashboardListSampleItem.css";
import moment from "moment";
class DashboardListSampleItem extends Component {
  render() {
    var { sample,match} = this.props;
   
    return (
      <tr>
        <td>{sample.code}</td>
        <td>{sample.collector}</td>
        <td>{sample.deliverer}</td>
        <td>{sample.receiver}</td>
        <td>{moment(sample.startDate).format("DD/MM/YYYY, hh:mm a")}</td>
        <td>{moment(sample.endDate).format("DD/MM/YYYY, hh:mm a")}</td>
        <td>
          <div
            className={`status-wrapper ${
              sample.status === true
                ? "done"
                : sample.status === false
                ? "notyet"
                : "doing"
            }`}
          >
            {sample.status === true
              ? "Done"
              : sample.status === false
              ? "Not yet"
              : "Doing"}
          </div>
        </td>
        <td>
          <div className="action-wrapper d-flex justify-content-center align-items-center">
            <div className="edit-btn mr-2">
              <NavLink to={`/dashboard/code/${match.params.codeid}/sample/${sample.id}`}>
                Add Process
              </NavLink>
            </div>
            <div className="view-btn">
              <NavLink to={`/dashboard/code/${match.params.codeid}/sample/${sample.id}/view`}>
                View Info
              </NavLink>
            </div>
          </div>
        </td>
      </tr>
    );
  }
}

export default DashboardListSampleItem;
