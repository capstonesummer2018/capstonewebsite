import React, { Component } from 'react';
import "./StepPRC.css"
class StepPRC extends Component {
    
    render() {
      if (this.props.currentStep !== 2) {
        return null
      } 
        return (
            <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              className="form-control"
              id="username"
              name="username"
              type="text"
              placeholder="Enter username"
              value={this.props.username} // Prop: The email input data
              onChange={this.props.handleChange} // Prop: Puts data into state
            />
          </div>
        );
    }
}

export default StepPRC;