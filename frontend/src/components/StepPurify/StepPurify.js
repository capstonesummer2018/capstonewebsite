import React, { Component, Fragment } from "react";
import "./StepPurify.css";
class StepPurify extends Component {
  render() {
    if (this.props.currentStep !== 4) {
      return null;
    }
    return (
      <Fragment>
        <div className="form-group">
          <label htmlFor="phone">Phone</label>
          <input
            className="form-control"
            id="phone"
            name="phone"
            type="text"
            placeholder="Enter phone"
            value={this.props.phone} // Prop: The email input data
            onChange={this.props.handleChange} // Prop: Puts data into state
          />
        </div>
        {/* <button className="btn btn-success btn-block">Sign up</button> */}
      </Fragment>
    );
  }
}

export default StepPurify;
