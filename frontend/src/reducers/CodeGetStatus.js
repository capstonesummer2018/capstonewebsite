import * as Types from "./../constants/ActionTypes";

var initialState = {};
const myReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.UPDATE_CODE_STATUS:
      state = action.statusCode;
      return state;
    default:
      return state;
  }
};

export default myReducer;
