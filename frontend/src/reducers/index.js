import { combineReducers } from "redux";
import codelist from "./CodeAction";
import filter from "./CodeActionSearch";
import filtersample from "./SampleActionSearch";
import samplelistbycodeid from "./SampleAction";
import codeiditem from "./CodeActionGetIdItem";
// import statuscodeid from "./CodeGetStatus";
import sampleiditem from "./SampleViewAction"
// import statussampleid from "./SampleGetStatus"
const myReducer = combineReducers({
  codelist,
  filter,
  samplelistbycodeid,
  codeiditem,
  sampleiditem,
  // statussampleid,
  filtersample,
  // statuscodeid
});

export default myReducer;
