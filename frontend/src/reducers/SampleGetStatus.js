import * as Types from "./../constants/ActionTypes";

var initialState = {
  status: null
};
const myReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_SAMPLE_STATUS:
      state = action.sampleStatus;
      return state;
    default:
      return state;
  }
};

export default myReducer;
