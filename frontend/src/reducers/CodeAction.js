import * as Types from "./../constants/ActionTypes";
import { findIndex } from "lodash";
var initialState = [];
const myReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_CODE:
      state = action.codelist;
      return [...state];
    
    case Types.ADD_CODE:
      state.push(action.code);
      return [...state];
    case Types.DELETE_CODE:
      var index = findIndex(state, code => {
        return code.id === action.id;
      });

      state.splice(index, 1);
      return [...state];
    default:
      return [...state];
  }
};

export default myReducer;
