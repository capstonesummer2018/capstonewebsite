import * as Types from "./../constants/ActionTypes";

var initialState = {};
const myReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_CODE_ID:
      state = action.codeIdItem;

      return state;

    default:
      return state;
  }
};

export default myReducer;
