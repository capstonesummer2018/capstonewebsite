﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CapstoneProjectWeb.Startup))]
namespace CapstoneProjectWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
